var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .addEntry('bootstrap', './node_modules/bootstrap/scss/bootstrap.scss')
    .addEntry('jquery', './node_modules/jquery/src/jquery.js')
    .addStyleEntry('style', './assets/css/app.css')
    .addStyleEntry('datetime-picker', './node_modules/react-datepicker/dist/react-datepicker.css')
    .cleanupOutputBeforeBuild()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader()
    .enableReactPreset()

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you use Sass/SCSS files
    .enableSassLoader()

// uncomment if you're having problems with a jQuery plugin
//.autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();