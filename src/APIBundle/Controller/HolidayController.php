<?php

namespace APIBundle\Controller;

use APIBundle\Model\Listing;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use SharedBundle\Entity\Holiday;
use SharedBundle\Form\Type\HolidayType;
use SharedBundle\Services\HolidayService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HolidayController extends FOSRestController
{
    /**
     * @ApiDoc()
     * @Rest\QueryParam(name="limit", requirements="\d+", default=5)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true)
     * @param ParamFetcherInterface $paramFetcher
     * @return Listing
     */
    public function getHolidaysAction(ParamFetcherInterface $paramFetcher)
    {
        $em = $this->get('doctrine')->getManagerForClass(Holiday::class);
        $repository = $em->getRepository(Holiday::class);

        /** @var QueryBuilder $qb */
        $qb = $repository->createQueryBuilder('holiday');
        $qb->andWhere('holiday.isDelete = FALSE');

        $limit = $paramFetcher->get('limit');
        if ($limit != null) {
            $qb->setMaxResults($limit);
        }

        $offset = $paramFetcher->get('offset');
        if ($offset != null) {
            $qb->setFirstResult($offset);
        }
        $paginator = new Paginator($qb);

        return new Listing($paginator);
//        TODO: refactor this with repository filter
    }

    /**
     * @ApiDoc()
     * @param $id
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getHolidayAction($id)
    {
        if ($id === '{id}') {
            throw new BadRequestHttpException('The parameter "id" is required!');
        }

        $em = $this->get('doctrine')->getManagerForClass(Holiday::class);
        $repository = $em->getRepository(Holiday::class);

        /** @var QueryBuilder $qb */
        $qb = $repository->createQueryBuilder('holiday');
        $qb->andWhere('holiday.id = :id')->setParameter('id', $id);

        return $qb->getQuery()->getSingleResult();
    }

    /**
     * @ApiDoc(
     *     input="SharedBundle\Form\Type\HolidayType"
     * )
     * @param Request $request
     * @param null $id
     * @return Holiday|JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postHolidayAction(Request $request, $id = null)
    {
        if (empty($request->request->all())) {
            throw new BadRequestHttpException('Empty data not accepted!');
        }

        $holidayService = $this->get(HolidayService::class);
        if ((int)$id !== 0) {
            $holiday = $holidayService->findById((int)$id);
        } else {
            $holiday = new Holiday();
        }

        $form = $this->createForm(HolidayType::class, $holiday);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $holidayService->save($holiday);
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $errors = [];
            foreach ($form->getErrors(true) as $error) {
                $errors['errors'][] = $error->getMessage();
            }

            return new JsonResponse($errors, 422);
        }

        return $holiday;
    }

    /**
     * @ApiDoc()
     * @param $id
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteHolidayAction($id)
    {
        $holidayService = $this->get(HolidayService::class);

        $holiday = $holidayService->findById($id);

        if (!$holiday) {
            throw new NotFoundHttpException(sprintf('Holiday with id %s not found.', $id));
        }

        $holidayService->delete($holiday);

        return new JsonResponse(['message' => sprintf('Holiday with id %s is deleted.', $id)]);
    }
}
