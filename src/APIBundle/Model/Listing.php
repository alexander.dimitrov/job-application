<?php
/**
 * Created by PhpStorm.
 * User: bozhidar.hristov
 * Date: 10/15/17
 * Time: 3:43 PM
 */

namespace APIBundle\Model;


use Doctrine\ORM\Tools\Pagination\Paginator;

class Listing
{
    private $data;
    private $total;

    /**
     * Listing constructor.
     * @param Paginator $paginator
     */
    public function __construct(Paginator $paginator)
    {
        $this->data = $paginator->getIterator()->getArrayCopy();
        $this->total = count($paginator);
    }
}