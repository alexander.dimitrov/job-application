<?php

namespace SharedBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class DateTimePeriod extends Constraint
{
    public $message = 'Date to can not be before today!';
}