<?php

namespace SharedBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Holiday;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateTimePeriodValidator extends ConstraintValidator
{
    /**
     * @param Holiday $holiday
     * @param Constraint $constraint
     * @return mixed
     */
    public function validate($holiday, Constraint $constraint)
    {
        if ($holiday->getPeriodTo() < $holiday->getPeriodFrom()) {
            $this->context->addViolation($constraint->message);
        }
    }
}