<?php

namespace SharedBundle\Form\Type;

use SharedBundle\Entity\Holiday;
use SharedBundle\Validator\Constraints\DateTimePeriod;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class HolidayType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('POST');
        $builder->add('name',
            TextType::class,
            [
                'constraints' => [
                    new NotBlank(['message' => 'Name field should not be blank'])
                ]
            ]
        );
        $builder->add('periodFrom',
            DateTimeType::class,
            [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(['message' => 'Date from field should not be blank'])
                ]
            ]
        );
        $builder->add('periodTo',
            DateTimeType::class,
            [
                'widget' => 'single_text',
                'constraints' => [
                    new NotBlank(['message' => 'Date to field should not be blank'])
                ]
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault('data_class', Holiday::class);
        $resolver->setDefault('constraints', [new DateTimePeriod()]);
        $resolver->setDefault('csrf_protection', false);
        $resolver->setDefault('allow_extra_fields', true);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
