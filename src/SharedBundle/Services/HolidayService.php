<?php
/**
 * Created by PhpStorm.
 * User: omfg
 * Date: 9/13/2018
 * Time: 7:07 PM
 */

namespace SharedBundle\Services;


use Doctrine\ORM\EntityManager;
use SharedBundle\Entity\Holiday;

class HolidayService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * HolidayService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Holiday $holiday
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Holiday $holiday)
    {
        if (!$holiday->getId()) {
            $this->em->persist($holiday);
        }

        $this->em->flush($holiday);
    }

    /**
     * @param $id
     * @return null|object|Holiday
     */
    public function findById($id)
    {
        return $this->em->getRepository(Holiday::class)->find($id);
    }

    /**
     * @param Holiday $holiday
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete(Holiday $holiday)
    {
        $holiday->setIsDelete(true);
        $this->em->flush($holiday);
    }
}