<?php

namespace SharedBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hilidays
 *
 * @ORM\Table(name="holidays")
 * @ORM\Entity(repositoryClass="SharedBundle\Repository\HolidayRepository")
 */
class Holiday
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="periodFrom", type="datetime")
     */
    private $periodFrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="periodTo", type="datetime")
     */
    private $periodTo;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_delete", type="boolean")
     */
    private $isDelete = false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Holiday
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set periodFrom
     *
     * @param \DateTime $periodFrom
     *
     * @return Holiday
     */
    public function setPeriodFrom($periodFrom)
    {
        $this->periodFrom = $periodFrom;

        return $this;
    }

    /**
     * Get periodFrom
     *
     * @return \DateTime
     */
    public function getPeriodFrom()
    {
        return $this->periodFrom;
    }

    /**
     * Set periodTo
     *
     * @param \DateTime $periodTo
     *
     * @return Holiday
     */
    public function setPeriodTo($periodTo)
    {
        $this->periodTo = $periodTo;

        return $this;
    }

    /**
     * Get periodTo
     *
     * @return \DateTime
     */
    public function getPeriodTo()
    {
        return $this->periodTo;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }
}

