import React, {Component} from 'react';
import Modal from 'react-modal';
import moment from 'moment';
import DatePicker from 'react-datepicker';

Modal.setAppElement('#root')

//TODO: Move to CSS
const customStyles = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        width: '30%',
        overflow: 'visible'
    }
};

class Holiday extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            items: [],
            count: 0,
            baseUrl: $('[data-base-url]').data('url'),
            modalIsOpen: false,
            formData: {
                name: '',
                periodFrom: moment(),
                periodTo: moment()
            },
            holidayId: 0,
            alert: {
                type: '',
                messages: [],
                isShown: false
            }
        };

        this.deleteHoliday = this.deleteHoliday.bind(this);
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onHolidayFormChange = this.onHolidayFormChange.bind(this);
        this.handleHolidayFromChange = this.handleHolidayFromChange.bind(this);
        this.handleHolidayToChange = this.handleHolidayToChange.bind(this);
        this.postHoliday = this.postHoliday.bind(this);
        this.getHoliday = this.getHoliday.bind(this);
    }

    render() {
        const {error, isLoaded, items, count, formData, alert} = this.state;

        let isShown = alert.isShown ? '' : 'd-none'

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>Loading...</div>;
        } else {
            return (
                <div>
                    <Modal
                        isOpen={this.state.modalIsOpen}
                        onAfterOpen={this.afterOpenModal}
                        onRequestClose={this.closeModal}
                        style={customStyles}
                        contentLabel="Example Modal"
                    >
                        <h3>Edit holiday</h3>
                        <hr/>

                        <form onSubmit={this.postHoliday}>
                            <div className={'form-group'}>
                            </div>
                            <div className={'row'}>
                                <div className={'col-sm-12'}>
                                    <label>
                                        Name:
                                        <input type="text"
                                               name={'name'}
                                               value={this.state.formData.name}
                                               className={'form-control'}
                                               onChange={this.onHolidayFormChange}
                                        />
                                    </label>
                                </div>
                                <div className={'col-sm-6'}>
                                    <label>
                                        Date from:
                                        <DatePicker
                                            selected={moment(formData.periodFrom)}
                                            selectsStart
                                            startDate={moment(formData.periodFrom)}
                                            endDate={moment(formData.periodTo)}
                                            onChange={this.handleHolidayFromChange}
                                            todayButton={"Today"}
                                            className={'form-control'}
                                        />
                                    </label>
                                </div>
                                <div className={'col-sm-6'}>
                                    <label>
                                        Date to:
                                        <DatePicker
                                            selected={moment(formData.periodTo)}
                                            selectsEnd
                                            startDate={moment(formData.periodFrom)}
                                            endDate={moment(formData.periodTo)}
                                            onChange={this.handleHolidayToChange}
                                            todayButton={"Today"}
                                            className={'form-control'}
                                        />
                                    </label>
                                </div>
                            </div>
                            <div className={'row mt-2'}>
                                <div className={'col-sm-6'}>
                                    <button type={'submit'} className={'btn btn-primary w-100-p'}>Save</button>
                                </div>
                                <div className={'col-sm-6'}>
                                    <button type={'button'} className={'btn btn-danger w-100-p'}
                                            onClick={this.closeModal}>Close
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div className={isShown + ' alert alert-' + alert.type + ' mt-3'}>
                            <ul>
                                {alert.messages.map(message => (<li key={message}>{message}</li>))}
                            </ul>
                        </div>
                    </Modal>

                    <div className={'row'}>
                        <div className={'col-sm-6'}>
                            <div className={'ml-3'}>
                                <h2>Holidays list</h2>
                                <p>Total results: {count}</p>
                            </div>
                        </div>
                        <div className={'col-sm-6'}>
                            <button type={'button'}
                                    className={'btn btn-primary float-right mr-3'}
                                    onClick={() => this.openModal(0)}>
                                Add new
                            </button>
                        </div>
                        <div className={'col-sm-12'}>
                            <table className={'table'}>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Date from</th>
                                    <th>Date to</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                {this.formatList(items)}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            );
        }
    }

    componentDidMount() {
        this.getHolidays()
        const {name, periodFrom, periodTo} = this.props;
        this.setState({
            formData: {
                name: name,
                periodFrom: periodFrom,
                periodTo: periodTo
            }
        });
    }

    getHolidays() {
        $.ajax({
            method: 'GET',
            url: this.state.baseUrl + 'api/v1/holidays.json?limit=20',
            success: function (response) {
                this.setState({
                    isLoaded: true,
                    items: response.data,
                    count: response.total
                });
            }.bind(this),
            error: function (xhr, status, error) {
                this.setState({
                    isLoaded: true,
                    error
                });
            }
        });
    }

    getHoliday(id) {
        if (id) {
            $.ajax({
                method: 'GET',
                url: this.state.baseUrl + 'api/v1/holidays/' + id + '.json',
                success: function (response) {
                    console.log(response)
                    this.setState({
                        formData: {
                            name: response.name,
                            periodFrom: response.period_from,
                            periodTo: response.period_to
                        }
                    })
                }.bind(this),
                error: function (xhr, status, error) {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }.bind(this)
            });
        }
    }

    onHolidayFormChange(e) {
        let {target: {name, value}} = e;
        this.setState({
            formData: {
                [name]: value,
                periodFrom: this.state.formData.periodFrom,
                periodTo: this.state.formData.periodTo
            }
        })
        ;
    }

    handleHolidayFromChange(periodFrom) {
        this.setState({
            formData: {
                name: this.state.formData.name,
                periodFrom: moment(periodFrom),
                periodTo: this.state.formData.periodTo
            }
        });
    }

    handleHolidayToChange(periodTo) {
        this.setState({
            formData: {
                name: this.state.formData.name,
                periodFrom: this.state.formData.periodFrom,
                periodTo: moment(periodTo)
            }
        });
    }


    postHoliday(e) {
        e.preventDefault();
        const {name, periodFrom, periodTo} = this.state.formData;
        const {holidayId} = this.state;

        $.ajax({
            method: 'POST',
            url: this.state.baseUrl + 'api/v1/holidays/' + holidayId + '.json',
            data: {
                name: name,
                periodFrom: moment(periodFrom).format('Y-MM-DD HH:mm:ss'),
                periodTo: moment(periodTo).format('Y-MM-DD HH:mm:ss'),
            },
            success: function () {
                this.getHolidays();
                if (holidayId === 0) {
                    this.setState({
                        alert: {
                            type: 'success',
                            messages: ['Holiday added successfully!'], //TODO: get message from response
                            isShown: true
                        }
                    })
                } else {
                    this.setState({
                        alert: {
                            type: 'success',
                            messages: ['Holiday has edited successfully!'], //TODO: get message from response
                            isShown: true
                        }
                    })
                }

            }.bind(this),
            error: function (xhr) {
                //
                this.setState({
                    alert: {
                        type: 'danger',
                        messages: xhr.responseJSON.errors,
                        isShown: true
                    }
                })
            }.bind(this)
        });
    }

    deleteHoliday(id) {
        if (confirm('Are you sure want delete this row ?')) {
            $.ajax({
                method: 'DELETE',
                url: this.state.baseUrl + 'api/v1/holidays/' + id + '.json',
                success: function () {
                    this.getHolidays();
                }.bind(this),
                error: function (xhr, status, error) {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            });
        }
    }

    formatList(items) {
        return (
            items.map(item => (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.period_from}</td>
                    <td>{item.period_to}</td>
                    <td>
                        <button type={'button'}
                                onClick={() => this.openModal(item.id)}
                                className={'btn btn-primary btn-sm'}>Edit
                        </button>
                    </td>
                    <td>
                        <button type={'button'}
                                onClick={() => this.deleteHoliday(item.id)}
                                className={'btn btn-danger btn-sm'}>Delete
                        </button>
                    </td>
                </tr>
            ))
        )
    }

    openModal(id) {
        if (id === 0) {
            this.setState({
                formData: {
                    name: '',
                    periodFrom: moment(),
                    periodTo: moment()
                }
            })
        } else {
            this.getHoliday(id);
        }
        this.setState({
            modalIsOpen: true,
            holidayId: id,
            alert: {
                type: '',
                messages: [],
                isShown: false
            }
        });
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }
}

export default Holiday;