import React from 'react';
import ReactDom from 'react-dom';
import Holiday from './Holiday/Holiday';

ReactDom.render(<Holiday/>, document.getElementById('root'));