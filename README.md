job-application
===============

A Symfony project created on September 13, 2018, 1:10 pm.

Installation
------------
First you need to install your project
> `composer install`

after that you need to create your database and create tables

> `php bin/console doctrine:database:create`

> `php bin/console doctrine:schema:update --force`

Then open the project in your browser and have fun!
